PImage canvasState;
int[] nextState;
int H;
int W;
boolean shouldDraw;
int ALIVE = #ffffff;
int DEAD = #000000;
int introTime = 5;

void setup() {
  size(512, 512);
  canvasState = loadRaster(512);
  canvasState.loadPixels();
  W = canvasState.width;
  H = canvasState.height;
  nextState = new int[canvasState.height * canvasState.width];
  arrayCopy(canvasState.pixels, nextState);
  frameRate(2);
  shouldDraw = true;
}

void draw() {
  //if (!shouldDraw) return;
  
  image(canvasState, 0, 0);
  if (introTime > 0) {
    introTime--;
    return; 
  }
  gameOfLife(canvasState.pixels, nextState, W, H);
  canvasState.updatePixels();
  shouldDraw = false;
}

void gameOfLife(int[] current, int[] next, int w, int h) {
  int[] neighbors = new int[current.length];
  for (int i = 0; i < current.length; i++) {
    int n = neighbors(current, i, w, h);
    if (n < 2) {
      next[i] = DEAD;
    } else if (current[i] == ALIVE && (n == 2 || n == 3)) {
      next[i] = ALIVE;
    } else if (current[i] == ALIVE && n > 3) {
      next[i] = DEAD;
    } else if (current[i] == DEAD && n == 3) {
      next[i] = ALIVE;
    }
    neighbors[i] = n;
  }

  arrayCopy(next, current);
}

int neighbors(int[] seq, int pos, int w, int h) {
  int x = pos % w;
  int y = pos / w;
  int neighbors = 0;
  for (int i = -1; i <= 1; i++) {
    for (int j = 1; j >= -1; j--) {
      int ix = (y + j) * w + x + i;
      if ((x + i >= 0 && x + i < w) && (y + j >= 0 && y + j < h) && !(i == 0 && j == 0)) {
        neighbors += seq[ix] == ALIVE ? 1 : 0;
      }
    }
  }
  return neighbors;
}

PImage loadRaster(int maxPixel) {
  PImage img = loadImage("Wisdom.png");
  img.filter(GRAY);
  int w, h;
  if (img.width > img.height) {
    w = maxPixel;
    h = img.height * maxPixel / img.width;
  } else {
    w = img.width * maxPixel / img.height;
    h = 256;
  }
  img.resize(w, h);
  img.loadPixels();
  for (int i = 0; i < img.pixels.length; i++) {
    if (luminance(img.pixels[i]) > 0.5) {
      img.pixels[i] = #ffffff;
    } else {
      img.pixels[i] = #000000;
    }
  }
  img.updatePixels();
  return img;
}

float luminance(color c) {
  float r = red(c)/255.0;
  float g = green(c)/255.0;
  float b = blue(c)/255.0;
  return (0.299*r + 0.587*g + 0.114*b);
}

void mouseClicked() {
  shouldDraw = true;
}
